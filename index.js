// Hàm thực hiện chức năng 1: Tổng các số dương trong mảng
function tinhTongSoDuong() {
  var numbers = getNumberArray();
  var sum = 0;
  for (var i = 0; i < numbers.length; i++) {
    if (numbers[i] > 0) {
      sum += numbers[i];
    }
  }
  document.getElementById("result").innerHTML =
    "Tổng các số dương trong mảng: " + sum;
}

// Hàm thực hiện chức năng 2: Đếm có bao nhiêu số dương trong mảng
function demSoDuong() {
  var numbers = getNumberArray();
  var count = 0;
  for (var i = 0; i < numbers.length; i++) {
    if (numbers[i] > 0) {
      count++;
    }
  }
  document.getElementById("result").innerHTML =
    "Số lượng số dương trong mảng: " + count;
}

// Hàm thực hiện chức năng 3: Tìm số nhỏ nhất trong mảng
function timSoNhoNhat() {
  var numbers = getNumberArray();
  var min = numbers[0];
  for (var i = 1; i < numbers.length; i++) {
    if (numbers[i] < min) {
      min = numbers[i];
    }
  }
  document.getElementById("result").innerHTML =
    "Số nhỏ nhất trong mảng: " + min;
}

// Hàm thực hiện chức năng 4: Tìm số dương nhỏ nhất trong mảng
function timSoDuongNhoNhat() {
  var numbers = getNumberArray();
  var minPositive = -1;
  for (var i = 0; i < numbers.length; i++) {
    if (numbers[i] > 0) {
      if (minPositive === -1 || numbers[i] < minPositive) {
        minPositive = numbers[i];
      }
    }
  }
  document.getElementById("result").innerHTML =
    "Số dương nhỏ nhất trong mảng: " + minPositive;
}

// Hàm thực hiện chức năng 5: Tìm số chẵn cuối cùng trong mảng
function timSoChanCuoiCung() {
  var numbers = getNumberArray();
  var lastEven = -1;
  for (var i = numbers.length - 1; i >= 0; i--) {
    if (numbers[i] % 2 === 0) {
      lastEven = numbers[i];
      break;
    }
  }
  document.getElementById("result").innerHTML =
    "Số chẵn cuối cùng trong mảng: " + lastEven;
}

// Hàm thực hiện chức năng 6: Đổi chỗ 2 giá trị trong mảng
function doiCho() {
  var numbers = getNumberArray();
  var position1 = parseInt(document.getElementById("position1").value);
  var position2 = parseInt(document.getElementById("position2").value);

  if (
    position1 >= 0 &&
    position1 < numbers.length &&
    position2 >= 0 &&
    position2 < numbers.length
  ) {
    var temp = numbers[position1];
    numbers[position1] = numbers[position2];
    numbers[position2] = temp;

    document.getElementById("result").innerHTML =
      "Mảng sau khi đổi chỗ: " + numbers.join(", ");
  } else {
    document.getElementById("result").innerHTML = "Vị trí không hợp lệ!";
  }
}

// Hàm thực hiện chức năng 7: Sắp xếp mảng theo thứ tự tăng dần
function sapXepTangDan() {
  var numbers = getNumberArray();
  numbers.sort(function (a, b) {
    return a - b;
  });
  document.getElementById("result").innerHTML =
    "Mảng sau khi sắp xếp: " + numbers.join(", ");
}

// Hàm kiểm tra số nguyên tố
function isPrime(number) {
  if (number < 2) {
    return false;
  }
  for (var i = 2; i <= Math.sqrt(number); i++) {
    if (number % i === 0) {
      return false;
    }
  }
  return true;
}

// Hàm thực hiện chức năng 8: Tìm số nguyên tố đầu tiên trong mảng
function timSoNguyenToDauTien() {
  var numbers = getNumberArray();
  for (var i = 0; i < numbers.length; i++) {
    if (isPrime(numbers[i])) {
      document.getElementById("result").innerHTML =
        "Số nguyên tố đầu tiên trong mảng: " + numbers[i];
      return;
    }
  }
  document.getElementById("result").innerHTML = "Mảng không có số nguyên tố!";
}

// Hàm thực hiện chức năng 9: Tìm số nguyên trong mảng số thực
function timSoNguyenTrongMangSoThuc() {
  var realNumbers = getRealNumberArray();
  var count = 0;
  for (var i = 0; i < realNumbers.length; i++) {
    if (Number.isInteger(realNumbers[i])) {
      count++;
    }
  }
  document.getElementById("result").innerHTML =
    "Số lượng số nguyên trong mảng số thực: " + count;
}

// Hàm thực hiện chức năng 10: So sánh số lượng số dương và số lượng số âm
function soSanhSoDuongVaSoAm() {
  var numbers = getNumberArray();
  var countPositive = 0;
  var countNegative = 0;
  for (var i = 0; i < numbers.length; i++) {
    if (numbers[i] > 0) {
      countPositive++;
    } else if (numbers[i] < 0) {
      countNegative++;
    }
  }
  if (countPositive > countNegative) {
    document.getElementById("result").innerHTML = "Số dương nhiều hơn số âm.";
  } else if (countPositive < countNegative) {
    document.getElementById("result").innerHTML = "Số âm nhiều hơn số dương.";
  } else {
    document.getElementById("result").innerHTML =
      "Số dương và số âm bằng nhau.";
  }
}

// Hàm lấy giá trị từ ô nhập số và trả về mảng số nguyên
function getNumberArray() {
  var input = document.getElementById("numberArray").value;
  var numbers = input.split(",").map(function (number) {
    return parseInt(number.trim());
  });
  return numbers;
}

// Hàm lấy giá trị từ ô nhập số thực và trả về mảng số thực
function getRealNumberArray() {
  var input = document.getElementById("realNumberArray").value;
  var numbers = input.split(",").map(function (number) {
    return parseFloat(number.trim());
  });
  return numbers;
}
